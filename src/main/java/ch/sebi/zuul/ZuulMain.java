package ch.sebi.zuul;

import ch.sebi.zuul.logic.Color;
import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.IO;
import ch.sebi.zuul.logic.MainGame;

import java.util.Random;

/**
 * The class containing the main method
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class ZuulMain {
    /**
     * the main function
     * @param args the args from the command line
     */
    public static void main(String[] args) {
        if(args.length == 0 || !args[0].equalsIgnoreCase("--no-fake-loading"))
            printFakeLoadingBar();
        MainGame mainGame = new MainGame();
        mainGame.play();
    }

    /**
     * Prints a fake loading bare on the screen
     */
    private static void printFakeLoadingBar() {
        Random random = new Random(System.currentTimeMillis());
        for (int i1 = 0; i1 <= 20; i1++) {
            IO.printf("\rLoad Game |");
            for (int i2 = 0; i2 <= i1; i2++)
                IO.printf("=");
            for (int i2 = i1; i2 < 20; i2++)
                IO.printf(" ");
            IO.printf("|");
            try {
                Thread.sleep(random.nextInt(300));
            } catch (InterruptedException e) {
            }
        }
        IO.printf("\r                                                \r");
    }
}
