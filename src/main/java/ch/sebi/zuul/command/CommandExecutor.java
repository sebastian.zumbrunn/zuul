package ch.sebi.zuul.command;

import ch.sebi.zuul.logic.Game;

/**
 * The command executor executes a command.
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
@FunctionalInterface
public interface CommandExecutor {
    /**
     * executes the command.
     *
     * @param words the words which the user entered
     * @param game  the game instance
     */
    void execute(String[] words, Game game);
}
