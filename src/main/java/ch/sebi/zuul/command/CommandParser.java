package ch.sebi.zuul.command;

import ch.sebi.zuul.exceptions.CommandNotFound;
import ch.sebi.zuul.logic.Game;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

/**
 * A class which parses lines in to commands and executes them
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class CommandParser {
    /**
     * the line parser
     */
    private Parser parser;
    /**
     * A hashmap with all commands mapped to their command word
     */
    private HashMap<String, CommandElement> commands = new LinkedHashMap<>();

    /**
     * constructor
     * @param parser the parser which should be used for parsing the lines
     */
    public CommandParser(Parser parser) {
        this.parser = parser;
    }

    /**
     * constructor
     * @param discoverer the discoveres which is used to discover the commands
     * @param parser the parser which should be used for parsing the lines
     */
    public CommandParser(CommandDiscoverer discoverer, Parser parser) {
        this(parser);
        discoverer.discover();
        CommandDiscoverer.DiscoveredCommand[] executors = discoverer.getExecutors();
        for (CommandDiscoverer.DiscoveredCommand e : executors) {
            registerExecutor(e.getCommandName(), e.getExecutor(), e.getSyntax(), e.getHelp());
        }
    }


    /**
     * parses the next line which is returned by {@link Parser#readNextLine()}.<br/>
     * This method blocks until the next line is found.
     * @param game the game instance
     * @throws IOException if an error occurred while reading the input stream
     * @see Parser#readNextLine()
     */
    public void parseNextCommand(Game game) throws IOException, CommandNotFound {
        String[] words = parser.readNextLine();
        if(words.length == 0) return;
        String cmdWord = words[0];
        CommandExecutor executor = getExecutor(cmdWord);
        if(executor == null) {
            throw new CommandNotFound(cmdWord);
        }
        executor.execute(words, game);
    }

    /**
     * Registers an executor for a command
     * @param cmd the command word
     * @param executor the executor
     */
    public void registerExecutor(String cmd, CommandExecutor executor, String syntax, String help) {
        CommandElement el = new CommandElement(cmd, executor, syntax, help);
        commands.put(cmd, el);
    }

    /**
     * Returns the registered executor for the command word or null if the command doesn't exist
     * @param cmd the command word
     * @return the executor or null
     */
    public CommandExecutor getExecutor(String cmd) {
        CommandElement el = commands.get(cmd);
        if(el == null) return null;
        return el.getExecutor();
    }

    /**
     * Returns the command element of a command.
     * @param cmd the command name
     * @return the command element
     */
    public CommandElement getCommand(String cmd) {
        return commands.get(cmd);
    }


    /**
     * Returns all commands which are registerd in this command parser
     * @return all commands known to this command parser
     */
    public CommandExecutor[] getExecutors() {
        return Arrays.stream(getCommands()).map(c -> c.getExecutor()).collect(Collectors.toList()).toArray(new CommandExecutor[0]);
    }

    /**
     * Returns all command elements which are reigstered in this command parser
     * @return all command executors
     */
    public CommandElement[] getCommands() {
        return commands.values().toArray(new CommandElement[0]);
    }

    /**
     * An class wich contaions the command executor, the name, the syntax and the help of a command
     */
    public static class CommandElement {
        /**
         * The name of the command
         */
        private String commandName;
        /**
         * The executor of the command
         */
        private CommandExecutor executor;

        /**
         * The syntax of the command
         */
        private String syntax;

        /**
         * The help information of the command
         */
        private String help;

        /**
         * constructor
         * @param commandName the name of the command
         * @param executor the executor of the command
         * @param syntax the syntax of the command
         * @param help the help information of the command
         */
        private CommandElement(String commandName, CommandExecutor executor, String syntax, String help) {
            this.commandName = commandName;
            this.executor = executor;
            this.syntax = syntax;
            this.help = help;
        }

        /**
         * returns the command name
         * @return the name
         */
        public String getCommandName() {
            return commandName;
        }

        /**
         * Returns the executor of the command
         * @return the executor
         */
        public CommandExecutor getExecutor() {
            return executor;
        }

        /**
         * Returns the syntax of the command
         * @return the syntax
         */
        public String getSyntax() {
            return syntax;
        }

        /**
         * Returns the help information of the command
         * @return the help
         */
        public String getHelp() {
            return help;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CommandElement that = (CommandElement) o;
            return Objects.equals(commandName, that.commandName) &&
                    Objects.equals(executor, that.executor) &&
                    Objects.equals(syntax, that.syntax) &&
                    Objects.equals(help, that.help);
        }

        @Override
        public int hashCode() {
            return Objects.hash(commandName, executor, syntax, help);
        }
    }
}
