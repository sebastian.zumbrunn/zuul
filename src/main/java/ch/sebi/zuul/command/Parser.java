package ch.sebi.zuul.command;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * A general parser which splits the input into words.
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class Parser {
    /**
     * the input stream which is used by the parser
     */
    private InputStreamReader input;

    /**
     * constructor
     *
     * @param in the input stream which is used by the parser
     */
    public Parser(InputStream in) {
        this.input = new InputStreamReader(in);
    }

    /**
     * Reads a line split in to words.
     * This method blocks until the line is entered.<br/>
     * A word are letters followed by a space, a tab or a new line.
     * If a new line character is found than the method returns all words collected until this point.
     *
     * @return the next line
     * @throws IOException if an io exception occurs while reading from the input stream
     */
    public String[] readNextLine() throws IOException {
        int currentInt;
        ArrayList<String> words = new ArrayList<>();
        String word = "";
        while ((currentInt = input.read()) != -1) {
            char currentChar = (char) currentInt;
            if (currentChar == ' ' || currentChar == '\t') {
                word = word.trim();
                if (!word.equals(""))
                    words.add(word);
                word = "";
            } else if (currentChar == '\n') {
                word = word.trim();
                if (!word.equals(""))
                    words.add(word);
                break;
            } else {
                word += currentChar;
            }
        }
        return words.toArray(new String[0]);
    }
}
