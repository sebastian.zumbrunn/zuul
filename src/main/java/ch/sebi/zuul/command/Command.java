package ch.sebi.zuul.command;

import java.lang.annotation.*;

/**
 * An annotation which marks an executor as an executor and can be found without registering the executor
 * directly in the {@link CommandParser}
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Repeatable(Commands.class)
public @interface Command {
    /**
     * The name of the command like "pickup".
     * It is the name which is used by the {@link CommandParser}
     * @return returns the name of the command
     * @see CommandParser#registerExecutor(String, CommandExecutor, String, String)
     */
    String value();

    /**
     * Returns the scope of the executor.
     * The default scope is "default".<br/><br/>
     *
     * The {@link CommandParser} only sources the executors which in the scope of command parser. The scope is a way
     * of saying that certain commands should only be avaible in certain command parsers
     * @return the scope of the command
     */
    String scope() default "default";
}
