package ch.sebi.zuul.command;

import org.reflections.Reflections;

import java.util.ArrayList;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Discovers commands with annotions
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class CommandDiscoverer {
    /**
     * logger
     */
    private Logger logger = Logger.getLogger(CommandDiscoverer.class.getName());
    /**
     * The scope of the command which should be found
     */
    private String scope;
    /**
     * In which package the discoverer should search for commands
     */
    private String searchPackage;

    /**
     * A list with the commands
     */
    private ArrayList<DiscoveredCommand> executors = new ArrayList<>();


    /**
     * constructor
     *
     * @param scope         the scope of the commands
     * @param searchPackage the package in which the discoverer should search
     */
    public CommandDiscoverer(String scope, String searchPackage) {
        this.scope = scope;
        this.searchPackage = searchPackage;
    }

    /**
     * Searches the given path after annotations
     */
    public void discover() {
        Reflections reflections = new Reflections(getSearchPackage());
        //handles if the Command Annotation occurs multiple times
        Set<Class<?>> annotateCommands = reflections.getTypesAnnotatedWith(Commands.class);
        for (Class<?> c : annotateCommands) {
            Commands commands = c.getAnnotation(Commands.class);
            for (Command command : commands.value()) {
                processCommand(command, c);
            }
        }
        //handles if the Command Annotation only occurs once
        Set<Class<?>> annotatedCommand = reflections.getTypesAnnotatedWith(Command.class);
        for (Class<?> c : annotatedCommand) {
            Command command = c.getAnnotation(Command.class);
            processCommand(command, c);
        }
    }

    /**
     * processes a command annotation
     * @param command the annotation
     * @param c the class from which the annotation came
     */
    private void processCommand(Command command, Class<?> c) {
        //if the scope doesn't match the scope of the discoverer, then the command isn't added
        if(!command.scope().equals(getScope())) return;
        try {
            Object obj = c.newInstance();
            if (!(obj instanceof CommandExecutor)) {
                logger.severe("CommandExecutor \"" + command.value() + "\" has to implement CommandExecutor");
                return;
            }
            String syntaxStr = command.value();
            String help = "";
            Syntax syntax = c.getAnnotation(Syntax.class);
            if (syntax != null) {
                syntaxStr = syntax.syntax();
                help = syntax.help();
            }
            executors.add(new DiscoveredCommand(
                    (CommandExecutor) obj,
                    command.value(),
                    command.scope(),
                    syntaxStr,
                    help
            ));
        } catch (InstantiationException e) {
            logger.severe("CommandExecutor \"" + command.value() + "\"has to have a empty constructor: " + e);
        } catch (IllegalAccessException e) {
            logger.severe("In CommandExecutor \"" + command.value() + "\"Cannot access constructor: " + e);
        }
    }

    /**
     * Returns the scope of the commands which are discovered by this discoverer
     *
     * @return the scope
     */
    public String getScope() {
        return scope;
    }

    /**
     * Returns the package in which the discoverer searches
     *
     * @return the package
     */
    public String getSearchPackage() {
        return searchPackage;
    }

    /**
     * Returns all found commands.<br/>
     * This method should be invoked AFTER #discoverer()
     *
     * @return all found commands
     */
    public DiscoveredCommand[] getExecutors() {
        return executors.toArray(new DiscoveredCommand[0]);
    }

    /**
     * A class which contains more information about a command
     */
    public static class DiscoveredCommand {
        /**
         * The executor
         */
        private CommandExecutor executor;
        /**
         * The name of the command
         */
        private String commandName;
        /**
         * The scope of the command
         */
        private String scope;
        /**
         * The syntax of the command
         */
        private String syntax;
        /**
         * The help information of the command
         */
        private String help;

        /**
         * constructor
         *
         * @param executor    the executor
         * @param commandName The name of the command
         * @param scope       the scope of the command
         * @param syntax      the syntax of the command
         * @param help        the help of the command
         */
        public DiscoveredCommand(CommandExecutor executor, String commandName, String scope, String syntax, String help) {
            this.executor = executor;
            this.commandName = commandName;
            this.scope = scope;
            this.syntax = syntax;
            this.help = help;
        }

        /**
         * Returns the executor
         *
         * @return the executor
         */
        public CommandExecutor getExecutor() {
            return executor;
        }

        /**
         * Returns the command name
         *
         * @return the command name
         */
        public String getCommandName() {
            return commandName;
        }

        /**
         * Returns the scope
         *
         * @return the scope
         */
        public String getScope() {
            return scope;
        }

        /**
         * Returns the syntax
         *
         * @return the syntax
         */
        public String getSyntax() {
            return syntax;
        }

        /**
         * returns the help information
         *
         * @return the help information
         */
        public String getHelp() {
            return help;
        }
    }
}
