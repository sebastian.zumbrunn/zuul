package ch.sebi.zuul.command;

import java.lang.annotation.*;

/**
 * A class which is used by the jvm if the {@link Command} annotation is repeated
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Commands {
    /**
     * The repeated {@link Command} annotations
     * @return the annotations
     */
    Command[] value();
}
