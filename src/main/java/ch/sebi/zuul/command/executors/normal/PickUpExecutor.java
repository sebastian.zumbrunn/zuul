package ch.sebi.zuul.command.executors.normal;

import ch.sebi.zuul.command.Command;
import ch.sebi.zuul.command.CommandExecutor;
import ch.sebi.zuul.command.Syntax;
import ch.sebi.zuul.exceptions.IllegalSlotNumberException;
import ch.sebi.zuul.logic.Color;
import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.IO;
import ch.sebi.zuul.logic.map.Room;
import ch.sebi.zuul.logic.items.Inventory;
import ch.sebi.zuul.logic.items.ItemSlot;

/**
 * The executor for the pickup command. <br/>
 * Syntax: pickup &lt;itemNr&gt; &lt;slotNr&gt; [&lt;count&gt];
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
@Command("pickup")
@Syntax(syntax = "pickup [<itemNr> [<slotNr> [<count>]]]", help = "picks up an item in the room")
public class PickUpExecutor implements CommandExecutor {
    private final static String SYNTAX = "pickup <itemNr> <slotNr> [<count>]";

    @Override
    public void execute(String[] words, Game game) {
        if (words.length >= 5) {
            IO.println(Color.RED, "ERROR: usage: " + SYNTAX);
            return;
        }
        Room current = game.getCurrentRoom();
        ItemSlot[] items = current.getItems();
        if(words.length == 1) {
            pickupAllItems(game);
        } else if(words.length > 1) {
            Inventory inventory = game.getPlayer().getInventory();
            int itemNr = 0;
            int slotNr = findUnusedSlot(inventory);
            int amount = 0;
            try {
                itemNr = Integer.parseInt(words[1]) - 1;
            } catch (NumberFormatException e) {
                IO.println(Color.RED, "ERROR: the 1nd parameter has to be a number");
                return;
            }
            if(itemNr < 0 || itemNr >= items.length) {
                IO.println(Color.RED, "ERROR: the item nr has to be between 1 and " + items.length);
                return;
            }
            ItemSlot itemSlot = items[itemNr];
            amount = itemSlot.getAmount();
            if(words.length > 2) {
                try {
                    slotNr = Integer.parseInt(words[2]) - 1;
                } catch (NumberFormatException e) {
                    IO.println(Color.RED, "ERROR: the 2nd parameter has to be a number");
                    return;
                }
            }
            if(words.length > 3)  {
                try {
                     amount = Integer.parseInt(words[3]);
                } catch (NumberFormatException e) {
                    IO.println(Color.RED, "ERROR: the 3nd parameter has to be a number");
                    return;
                }
            }
            pickupItem(itemSlot, slotNr, amount, game);
            current.removeItem(itemSlot.getItem(), amount);
        }
    }

    /**
     * Picks up all items in the current room
     * @param game the game instance
     */
    private void pickupAllItems(Game game) {
        Inventory inventory = game.getPlayer().getInventory();
        Room current = game.getCurrentRoom();
        if(current.getItems().length == 0) {
            IO.println("There are no items to pickup");
            return;
        }
        for(ItemSlot itemSlot : current.getItems()) {
            pickupItem(itemSlot, findUnusedSlot(inventory), itemSlot.getAmount(), game);
            current.removeItem(itemSlot.getItem(), itemSlot.getAmount());
        }
    }

    /**
     * Picks up a specific item
     * @param itemSlot the item slot of the room
     * @param slotNr the slot number of the inventory
     * @param amount the amount of the items which should be picked up
     * @param game the game instance
     */
    private void pickupItem(ItemSlot itemSlot, int slotNr, int amount, Game game) {
        Inventory inventory = game.getPlayer().getInventory();
        ItemSlot slot = inventory.getSlot(slotNr);
        if (slot == null) {
            try {
                inventory.setSlot(slotNr, itemSlot.getItem(), amount);
            } catch (IllegalSlotNumberException e) {
                IO.printf(Color.RED, "ERROR: the slot number %d has to be between 0 and %d\n", slotNr + 1, inventory.getMaxSlots());
            }
        } else {
            if(!slot.getItem().equals(itemSlot.getItem())) {
                IO.printf(Color.RED, "ERROR: the slot %d stores %s insteat of %s\n", slotNr + 1, slot.getItem().getName(), itemSlot.getItem().getName());
                return;
            }
            try {
                inventory.incrementSlot(slotNr, amount);
            } catch (IllegalSlotNumberException e) {
                e.printStackTrace();
            }
        }

        IO.printf("Picked up %s (%dx) and put it into inventory slot %d\n", itemSlot.getItem().getName(), amount, slotNr + 1);
    }

    /**
     * Finds an unused slot in the inventory
     * @param inventory the inventory
     * @return the slot
     */
    private int findUnusedSlot(Inventory inventory) {
        for (int i = 0; i < inventory.getMaxSlots(); i++) {
            if(inventory.getSlot(i) == null)
                return i;
        }
        return -1;
    }
}
