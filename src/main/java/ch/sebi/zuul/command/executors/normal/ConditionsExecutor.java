package ch.sebi.zuul.command.executors.normal;

import ch.sebi.zuul.command.Command;
import ch.sebi.zuul.command.CommandExecutor;
import ch.sebi.zuul.command.Syntax;
import ch.sebi.zuul.logic.Color;
import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.IO;
import ch.sebi.zuul.logic.condition.Condition;
import ch.sebi.zuul.logic.map.Exit;
import ch.sebi.zuul.logic.map.Room;

/**
 * The executor for the condition[s] command
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
@Command("conditions")
@Command("condition")
@Syntax(syntax = "condition[s] [<exitNr>]", help = "shows either all conditions of all exits or only of the given exit nr")
public class ConditionsExecutor implements CommandExecutor {
    private static final String SYNTAX = "conditions [<exit>]";

    @Override
    public void execute(String[] words, Game game) {
        if (words.length == 1) {
            printAllConditions(game.getCurrentRoom(), game);
        } else if (words.length == 2) {
            Exit e = game.getCurrentRoom().getExit(words[1]);
            if (e == null) {
                IO.println(Color.RED, "ERROR: exit " + words[1] + " doesn't exist");
            }
            printCondition(e, game);
        } else {
            IO.println(Color.RED, "ERROR: usage: " + SYNTAX);
        }
    }

    /**
     * prints all conditions
     *
     * @param current the current room
     * @param game    the game instance
     */
    private void printAllConditions(Room current, Game game) {
        IO.println("The conditions are:");
        for (Exit e : current.getExits()) {
            IO.println("\t- " + e.getName());
            for (Condition condition : e.getConditions()) {
                String checked = "(not fulfilled)";
                if (condition.condition(game))
                    checked = "(fulfilled)";
                IO.println("\t\t- " + condition.getText() + " " + checked);
            }
            if (e.getConditions().length == 0)
                IO.println("\t\t- no conditions");
        }
        IO.println();
    }

    /**
     * prints the conditions for the given exit
     *
     * @param e    the exit
     * @param game the game instance
     */
    private void printCondition(Exit e, Game game) {
        IO.println("The conditions for the exit " + e.getName() + " are:");
        for (Condition condition : e.getConditions()) {
            String checked = "(not fulfilled)";
            if (condition.condition(game))
                checked = "(fulfilled)";
            IO.println("\t- " + condition.getText() + " " + checked);
        }
        if (e.getConditions().length == 0)
            IO.println("\t- no conditions");
        IO.println();
    }
}
