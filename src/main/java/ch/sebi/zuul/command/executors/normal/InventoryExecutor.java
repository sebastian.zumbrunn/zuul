package ch.sebi.zuul.command.executors.normal;

import ch.sebi.zuul.command.Command;
import ch.sebi.zuul.command.CommandExecutor;
import ch.sebi.zuul.command.Syntax;
import ch.sebi.zuul.exceptions.IllegalSlotNumberException;
import ch.sebi.zuul.logic.Color;
import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.IO;
import ch.sebi.zuul.logic.items.*;
import ch.sebi.zuul.logic.player.Player;
import ch.sebi.zuul.logic.ui.input.IntInput;
import ch.sebi.zuul.logic.ui.menu.Menu;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * The inventory executor<br/>
 * syntax: inventory
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
@Command("inventory")
@Syntax(syntax = "inventory", help = "enters the inventory menu")
public class InventoryExecutor implements CommandExecutor {
    /**
     * The main inventory menu
     */
    private Menu menu;
    /**
     * if the inventory menu is still running
     */
    private boolean running = false;

    /**
     * constructor
     */
    public InventoryExecutor() {
        menu = new Menu();
        //the list slots menu
        menu.addMenu("List slots").setAction(game -> {
            IO.println();
            Player player = game.getPlayer();
            if(player.getEquipedWeapon1() != null) {
                IO.println("Weapon slot 1: " + player.getEquipedWeapon1().getName());
            }
            if(player.getEquipedWeapon2() != null) {
                IO.println("Weapon slot 1: " + player.getEquipedWeapon2().getName());
            }
            IO.println("The inventory slots:");
            Inventory inventory = game.getPlayer().getInventory();
            for (int i = 0; i < inventory.getMaxSlots(); i++) {
                ItemSlot slot = inventory.getSlot(i);
                if (slot != null) {
                    IO.printf("%d) %s (%dx) %s\n", i + 1, slot.getItem().getName(), slot.getAmount(), getItemStatis(slot.getItem()));
                }
            }
            IO.println();
        });

        //the use item menu
        menu.addMenu("use item").setAction(game -> {
            int slotNr = new IntInput("slotNr").printAndWait(game) - 1;
            ItemSlot slot = game.getPlayer().getInventory().getSlot(slotNr);
            if (slot == null) {
                IO.println(Color.RED, "ERROR: Invalid slot number");
                return;
            }
            Item item = slot.getItem();
            if(!(item instanceof Usable)) {
                IO.println(Color.RED, "ERROR: Item " + item.getName() + " is not usable");
                return;
            }
            Usable usable = (Usable) item;
            usable.use(game.getPlayer(), game);
            if(usable.destroyOnUse()) {
                try {
                    game.getPlayer().getInventory().incrementSlot(slotNr, -1);
                    IO.println("Item was used and destroyed.");
                } catch (IllegalSlotNumberException e) {
                    e.printStackTrace();
                }
            } else {
                IO.println("Item was used");
            }
        });

        // the equip item menu
        menu.addMenu("equip item").setAction(game -> {
            int sloNr = new IntInput("slotNr").printAndWait(game) - 1;
            Player player = game.getPlayer();
            Inventory inventory = game.getPlayer().getInventory();
            ItemSlot slot = inventory.getSlot(sloNr);
            if(slot == null) {
                IO.println(Color.RED, "ERROR: Invalid slot number");
                return;
            }
            if(!(slot.getItem() instanceof Weapon)) {
                IO.println(Color.RED, "ERROR: item " + slot.getItem().getName() + " isn't a weapon");
                return;
            }
            int weaponSlot = new IntInput("weapon(1,2)").printAndWait(game) - 1;
            if(weaponSlot != 0 && weaponSlot != 1) {
                IO.println(Color.RED, "ERROR: weapon has to be 1 or 2");
                return;
            }

            if(weaponSlot == 0) {
                player.setEquipedWeapon1((Weapon) slot.getItem());
            } else if(weaponSlot == 1) {
                player.setEquipedWeapon2((Weapon) slot.getItem());
            }
            IO.println("Equiped " + slot.getItem().getName() + " in slot " + (weaponSlot + 1));

        });

        // the drop item menu
        menu.addMenu("drop item").setAction((game) -> {
            int slotNr = new IntInput("slotNr.").printAndWait(game);
            try {
                game.getPlayer().getInventory().removeSlot(slotNr - 1);
            } catch (IllegalSlotNumberException e) {
                IO.println(Color.RED, "ERROR: Invalid slot number");
                return;
            }
            IO.printf("Droped items in slot %d\n", slotNr);
        });

        // the exit inventory menu
        menu.addMenu("exit menu").setAction((game) -> {
            IO.println("Exit inventory menu");
            running = false;
        });
    }

    @Override
    public void execute(String[] words, Game game) {
        IO.println();
        IO.println("Welcome to your inventory!");
        running = true;
        while (running) {
            IO.println("Choose the sub-menu:");
            menu.printAndWait(game);
            IO.println();
        }
    }

    /**
     * returns the item statis (like usable or weapon) joined by ',' and wrapped in parentheses
     * @param item the item which is used to get the statis
     * @return the joined statis
     */
    private String getItemStatis(Item item) {
        ArrayList<String> statis = new ArrayList<>();
        if(item instanceof Usable)  {
            statis.add("usable");
        }
        if(item instanceof Weapon) {
            statis.add("weapon");
        }
        return statis.stream().map(s -> "(" + s + ")").collect(Collectors.joining(", "));
    }
}
