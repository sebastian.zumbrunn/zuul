package ch.sebi.zuul.command.executors.normal;

import ch.sebi.zuul.command.Command;
import ch.sebi.zuul.command.CommandExecutor;
import ch.sebi.zuul.command.Syntax;
import ch.sebi.zuul.logic.Color;
import ch.sebi.zuul.logic.condition.Condition;
import ch.sebi.zuul.logic.map.Exit;
import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.IO;
import ch.sebi.zuul.logic.map.Room;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The executor for the go command
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
@Command(value = "go")
@Syntax(syntax = "go <room name>", help = "goes to a room")
public class GoExecutor implements CommandExecutor {
    @Override
    public void execute(String[] words, Game game) {
        if(words.length != 2) {
            IO.println(Color.RED, "ERROR: usage: go <room>");
            IO.println();
            return;
        }
        String room = Arrays.stream(Arrays.copyOfRange(words, 1, words.length)).collect(Collectors.joining(" ")).trim();
        Room currentRoom = game.getCurrentRoom();
        Exit exit = null;
        for (Exit e : currentRoom.getExits()) {
            if(e.getName().equalsIgnoreCase(room)) {
                exit = e;
            }
        }
        if(exit == null) {
            IO.printf(Color.RED, "ERROR: Room \"%s\" does not exist\n", room);
            IO.println();
            return;
        }

        if(!exit.isOpen(game)) {
            IO.println("This exit is currently closed");
            IO.println("You need:");
            for (Condition c : exit.getConditions()) {
                IO.println("\t- " + c.getText());
            }
            IO.println();
            return;
        }

        game.setCurrentRoom(exit.getRoom());
    }
}
