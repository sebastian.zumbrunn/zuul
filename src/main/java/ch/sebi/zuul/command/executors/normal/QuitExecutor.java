package ch.sebi.zuul.command.executors.normal;

import ch.sebi.zuul.command.Command;
import ch.sebi.zuul.command.CommandExecutor;
import ch.sebi.zuul.command.Syntax;
import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.IO;

/**
 * The executor for the quit command<br/>
 * syntax: quit
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
@Command("quit")
@Syntax(syntax = "quit", help = "quits the game")
public class QuitExecutor implements CommandExecutor {
    @Override
    public void execute(String[] words, Game game) {
        IO.println("Bey!");
        System.exit(0);
    }
}
