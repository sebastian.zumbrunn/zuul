package ch.sebi.zuul.command.executors.fight;

import ch.sebi.zuul.command.Command;
import ch.sebi.zuul.command.CommandExecutor;
import ch.sebi.zuul.command.Syntax;
import ch.sebi.zuul.logic.Color;
import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.IO;
import ch.sebi.zuul.logic.fight.Fight;
import ch.sebi.zuul.logic.items.Weapon;
import ch.sebi.zuul.logic.player.Player;
import ch.sebi.zuul.logic.ui.input.IntInput;

/**
 * The executor which handels the attack command in fights
 */
@Command(value = "attack", scope = "fight")
@Syntax(syntax = "attack [<enemyNr> <weaponSlot>]", help = "attacks the given enemy with the given weapon slot")
public class AttackExecutor implements CommandExecutor {
    /**
     * Syntax of the command
     */
    private final static String SYNTAX = "fight <enemyNr> <weaponSlot>";

    @Override
    public void execute(String[] words, Game game) {
        if (words.length != 3 && words.length != 1) {
            IO.println(Color.RED, "ERROR: Usage: " + SYNTAX);
            return;
        }
        Fight fight = game.getCurrentFight();
        Player[] enemies = game.getCurrentRoom().getEnemies();
        Player player = game.getPlayer();

        int enemyNr = 0;
        if (words.length == 3) {
            try {
                enemyNr = Integer.parseInt(words[1]) - 1;
            } catch (NumberFormatException e) {
                IO.println(Color.RED, "ERROR: EnemyNr has to be a number");
                return;
            }
        } else {
            enemyNr = new IntInput("enemyNr(1-" + enemies.length + ")").printAndWait(game) - 1;
        }
        if (enemyNr < 0 || enemyNr >= enemies.length) {
            IO.println(Color.RED, "ERROR: invalid enemy number");
            return;
        }
        Player enemy = enemies[enemyNr];

        int weaponSlot = 0;
        if(words.length == 3) {
            try {
                weaponSlot = Integer.parseInt(words[2]);
            } catch (NumberFormatException e) {
                IO.println(Color.RED, "ERROR: WeaponSlot has to be a number");
                return;
            }
        } else {
            weaponSlot = new IntInput("weaponSlot(1,2)").printAndWait(game);
        }
        Weapon weapon = null;
        if (weaponSlot == 1) {
            weapon = player.getEquipedWeapon1();
        } else if (weaponSlot == 2) {
            weapon = player.getEquipedWeapon2();
        } else {
            IO.println(Color.RED, "Weapon slot has to be 1 or 2");
            return;
        }
        if (weapon == null) {
            IO.println(Color.RED, "Weapon slot " + weaponSlot + " has to be equipped");
            return;
        }
        weapon.attack(enemy, player, game);
        IO.printf("The enemy %s has %d lifes left\n", enemy.getName(), enemy.getLife());
        if (enemy.isDead()) {
            game.getCurrentRoom().removeEnemy(enemy);
        }
        fight.nextEnemyTurn(game);
    }
}
