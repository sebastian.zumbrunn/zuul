package ch.sebi.zuul.command.executors.commons;

import ch.sebi.zuul.command.Command;
import ch.sebi.zuul.command.CommandExecutor;
import ch.sebi.zuul.command.CommandParser;
import ch.sebi.zuul.command.Syntax;
import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.IO;

import java.util.ArrayList;

/**
 * The executor for the help command<br/>
 * syntax: help
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
@Command("help")
@Command(value = "help", scope = "fight")
@Syntax(syntax = "help", help = "displays a help page")
public class HelpExecutor implements CommandExecutor {
    @Override
    public void execute(String[] words, Game game) {
        IO.println("You are lost. You are alone.");
        IO.println();
        IO.println("Following commands exist:");
        CommandParser parser = game.getCommandParser();
        if(game.getCurrentFight() != null)
            parser = game.getCurrentFight().getCommandParser();

        int maxLength = 0;
        for(CommandParser.CommandElement cmd : parser.getCommands()) {
            if(maxLength < cmd.getSyntax().length()) {
                maxLength = cmd.getSyntax().length();
            }
        }
        //handels if there are 2 conditions with the same syntax and help
        ArrayList<CommandParser.CommandElement> cmds = new ArrayList<>();
        for (CommandParser.CommandElement el1 : parser.getCommands()) {
            boolean alreadyExists = false;
            for (CommandParser.CommandElement el2 : cmds) {
                if(el1.getSyntax().equals(el2.getSyntax()) &&
                    el1.getHelp().equals(el2.getHelp())) {
                    alreadyExists = true;
                }
            }
            if (!alreadyExists) cmds.add(el1);
            if(cmds.size() == 0) {
                cmds.add(el1);
            }
        }

        for(CommandParser.CommandElement cmd : cmds)  {
            IO.printf("\t%-" + (maxLength + 3) + "s - %s\n", cmd.getSyntax(), cmd.getHelp());
        }
        IO.println();
    }

}
