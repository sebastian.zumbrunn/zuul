package ch.sebi.zuul.command;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A annotation which is used for defining the syntax of a command
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Syntax {
    /**
     * The syntax of the command.
     * It has to follow the format:<br/>
     * <ul>
     *     <li>arguments are wrapped in &lt; and &gt;</li> (like &lt;argument&gt;)
     *     <li>optional parts are wrapped in [ and ] (like [&lt;optional argument&gt;]) </li>
     * </ul>
     *
     * Example: pickup [&lt;itemNr&gt; [&lt;slotNr&gt; [&lt;amount&gt;]]]
     * @return the syntax of the command
     */
    String syntax();

    /**
     * Returns a general help for the command like "picks up items from a room"
     * @return a general help
     */
    String help() default "";
}
