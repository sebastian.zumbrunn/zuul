package ch.sebi.zuul.logic.ui;

import ch.sebi.zuul.logic.Game;

/**
 * A interface from which all "ui" classes should inherit.
 * @author Sebastian Zumbrunn
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public interface UiObj<T> {
    /**
     * Prints the ui object and executes the logic of the ui object
     * @param game the game instance
     * @return returns the result of the ui object or void/null if there is no result
     */
    T printAndWait(Game game);
}
