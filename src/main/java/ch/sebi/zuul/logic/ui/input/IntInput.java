package ch.sebi.zuul.logic.ui.input;

import ch.sebi.zuul.exceptions.InputConvertException;

/**
 * An input which takes an integer
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class IntInput extends Input<Integer> {
    /**
     * constructor<br/>
     * Sets the promt to "number"
     */
    public IntInput() {
        this("number");
    }

    /**
     * constructor<br/>
     * @param prompt the promt text of the input
     */
    public IntInput(String prompt) {
        super(prompt, true);
    }

    @Override
    protected Integer convertInput(String input) throws InputConvertException {
        try {
            return Integer.parseInt(input.trim());
        } catch (NumberFormatException e) {
            throw new InputConvertException("Enter a valid number", true, e);
        }
    }
}
