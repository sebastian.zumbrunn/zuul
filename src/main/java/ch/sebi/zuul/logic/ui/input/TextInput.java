package ch.sebi.zuul.logic.ui.input;

import ch.sebi.zuul.exceptions.InputConvertException;

/**
 * An input which takes text
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class TextInput extends Input<String>  {
    /**
     * constructor<br/>
     * Sets the prompt to "text"
     */
    public TextInput() {
        this("text");
    }

    /**
     * constructor<br/>
     * @param prompt the prompt text which is shown in the input
     */
    public TextInput(String prompt) {
        super(prompt, false);
    }

    @Override
    protected String convertInput(String input) throws InputConvertException {
        return input;
    }
}
