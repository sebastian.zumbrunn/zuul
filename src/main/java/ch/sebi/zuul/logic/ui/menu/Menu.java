package ch.sebi.zuul.logic.ui.menu;

import ch.sebi.zuul.logic.Color;
import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.IO;
import ch.sebi.zuul.logic.ui.UiObj;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * a class which implements a basic menu
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class Menu implements UiObj<MenuItem> {
    /**
     * a list of all items of the menu
     */
    private ArrayList<MenuItem> items = new ArrayList<>();

    /**
     * The item which was selected
     */
    private MenuItem selectedItem = null;

    /**
     * constructor<br/>
     *
     * Creates a new menu
     */
    public Menu() {

    }

    /**
     * constructor<br/>
     *
     * Creates a new menu and adds the given menu items to the Menu
     * @param items the items which are added
     */
    public Menu(MenuItem... items) {
        this.items.addAll(Arrays.asList(items));
    }

    /**
     * constructor<br/>
     *
     * Creates a new menu and adds the given strings as menu items.
     * The string is the title of the menu item
     * @param items the titles of the menu items
     */
    public Menu(String... items) {
        for (String i : items) {
            addMenu(i);
        }
    }

    /**
     * Adds a menu item to the menu
     * @param item the menu item to add
     * @return the menu item which was added
     */
    public MenuItem addMenu(MenuItem item) {
        items.add(item);
        return item;
    }

    /**
     * Adds a menu item to the menu.
     * @param title the title of the menu item
     * @return the menu item which was added
     */
    public MenuItem addMenu(String title) {
        return addMenu(new MenuItem(title));
    }

    /**
     * Returns all menu items of this menu in an array
     * @return the menu items
     */
    public MenuItem[] getItems() {
        return items.toArray(new MenuItem[0]);
    }

    @Override
    public MenuItem printAndWait(Game game) {
        clear();
        for(int i = 0; i < items.size(); i++) {
            MenuItem item = items.get(i);
            IO.getOut().printf("%d) %s\n", i + 1, item.getTitle());
        }
        try {
            boolean gotInput = false;
            while (!gotInput) {
                IO.printf("menu (1-%d)> ", items.size());
                String[] words = IO.getParser().readNextLine();
                if(words.length != 1) {
                    IO.println(Color.RED, "ERROR: Please enter the number of the menu you chose");
                    continue;
                }
                int menuId = 0;
                try {
                    menuId = Integer.parseInt(words[0]) - 1;
                } catch (NumberFormatException e) {
                    IO.println(Color.RED,"ERROR: Please enter the number of the menu you chose");
                    continue;
                }

                if(menuId < 0 || menuId > items.size()) {
                    IO.println(Color.RED,"ERROR: Please enter the number of the menu you chose");
                    continue;
                }
                selectedItem = items.get(menuId);
                MenuItemAction action  = selectedItem.getAction();
                if(action != null) {
                    action.invoke(game);
                }
                gotInput = true;
            }
        } catch (IOException e) {
            IO.println(Color.RED,"ERROR: Everything is suddenly black!!!! PLEASE HEL");
            System.exit(2);
        }
        return selectedItem;
    }

    /**
     * Returns the menu item which was selected in the last {@link #printAndWait(Game)}.
     * If either {@link #clear()} was called or {@link #printAndWait(Game)} wasn't called yet then this function
     * returns null
     * @return the menu item or null if it isn't set
     */
    public MenuItem getSelectedItem() {
        return selectedItem;
    }

    /**
     * Clears the selected menu item and sets it to null
     */
    public void clear() {
        selectedItem = null;
    }
}
