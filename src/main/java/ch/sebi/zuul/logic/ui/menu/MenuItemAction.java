package ch.sebi.zuul.logic.ui.menu;

import ch.sebi.zuul.logic.Game;

/**
 * An interface which is used for event listeners for {@link MenuItem}
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
@FunctionalInterface
public interface MenuItemAction {
    /**
     * This method is invoked if the menu to which it was registered was chosen
     * @param game the game instance
     */
    void invoke(Game game);
}
