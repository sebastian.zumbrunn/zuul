package ch.sebi.zuul.logic.ui.input;

import ch.sebi.zuul.exceptions.InputConvertException;
import ch.sebi.zuul.logic.Color;
import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.IO;
import ch.sebi.zuul.logic.ui.UiObj;

import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * A class which implements a basic input for integer
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public abstract class Input<T> implements UiObj<T> {
    /**
     * The prompt text
     */
    private String prompt;
    /**
     * The result which was selected/inserted
     */
    private T selectedResult = null;
    /**
     * If the input only consists of one word. If this field is true then the {@link #convertInput(String)}
     * is only invoked if one word is entered
     */
    private boolean onlyOneWord;

    /**
     * constructor
     * @param prompt the prompt text of the input
     * @param onlyOneWord if the input should only consists out of one word ({@link #onlyOneWord})
     */
    public Input(String prompt, boolean onlyOneWord) {
        this.prompt = prompt;
        this.onlyOneWord = onlyOneWord;
    }

    @Override
    public T printAndWait(Game game) {
        IO.printf(getPrompt() + ">");
        try {
            boolean gotResult = false;
            while(!gotResult) {
                String[] words = IO.getParser().readNextLine();
                if(onlyOneWord && words.length != 1) {
                    continue;
                }
                try {
                    selectedResult = convertInput(Arrays.stream(words).collect(Collectors.joining(" ")));
                } catch (InputConvertException e) {
                    if(e.isShowErrorMsg()) {
                        IO.println(Color.RED, "ERROR: " + e.getMessage());
                    }
                    continue;
                }
                gotResult = true;
            }
        } catch (IOException e) {
            IO.println(Color.RED, "ERROR: Everything is suddenly black!!!! PLEASE HEL");
            System.exit(2);
        }
        return selectedResult;
    }

    /**
     * Converts the entered text into the type of this input
     * @param input the input from the user
     * @return the converted input
     * @throws InputConvertException if an exception/error occurs while converting. Depending on the settings of {@link InputConvertException}
     *  is the error shown to the user
     */
    protected abstract T convertInput(String input) throws InputConvertException;

    /**
     * Returns the prompt text
     * @return the prompt text
     */
    public String getPrompt() {
        return prompt;
    }

    /**
     * Sets the prompt text
     * @param prompt the prompt text
     */
    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    /**
     * Returns the entered result
     * @return the entered result
     */
    public T getResult() {
        return selectedResult;
    }

    /**
     * Sets the entered result to null
     * @see #getResult()
     */
    public void clear() {
        selectedResult = null;
    }
}
