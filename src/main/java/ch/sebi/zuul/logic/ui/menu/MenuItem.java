package ch.sebi.zuul.logic.ui.menu;

/**
 * A Menuitem of {@link Menu}
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class MenuItem {
    /**
     * The counter for {@link #getNextId()}
     */
    private static int nextId = 0;
    /**
     * The unique id of this menu item
     */
    private int id;
    /**
     * The title of this menu item
     */
    private String title;
    /**
     * The action which should be executed if the item is chosen
     */
    private MenuItemAction action;

    /**
     * constructor <br/>
     * a new menu item
     * @param title the title of the menu item. It is shown to the user.
     */
    public MenuItem(String title) {
        this(title, null);
    }

    /**
     * constructor<br/>
     * a new menu item
     * @param title the title of the menu item. It is shown to the user.
     * @param action the action which is invoked if the menu item was chosen
     */
    public MenuItem(String title, MenuItemAction action) {
        this.id = getNextId();
        this.title = title;
        this.action = action;
    }

    /**
     * Returns the id.
     * The id is generated in the constructor {@link #getNextId()} and
     * is unique for every MenuItem:
     * @return the id of this menu item
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the tile of this menu item.
     * It is shown to the user in the menu.
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title which is shown to the user
     * @param title the new title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Returns hte action which is invoked if this menu is chosen
     * @return the action
     */
    public MenuItemAction getAction() {
        return action;
    }

    /**
     * Sets the action which is invoked if this menu item is chosen
     * @param action the action
     */
    public void setAction(MenuItemAction action) {
        this.action = action;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof MenuItem)) return false;
        MenuItem other = (MenuItem) obj;
        return other.getId() == getId();
    }

    /**
     * Returns a unique id.
     * It is a counter which is incremented with every call to this function
     * @return a new unique id
     */
    private static int getNextId() {
        return nextId++;
    }

}
