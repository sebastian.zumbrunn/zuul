package ch.sebi.zuul.logic.fight;

import ch.sebi.zuul.command.CommandDiscoverer;
import ch.sebi.zuul.command.CommandParser;
import ch.sebi.zuul.exceptions.CommandNotFound;
import ch.sebi.zuul.exceptions.IllegalSlotNumberException;
import ch.sebi.zuul.logic.Color;
import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.IO;
import ch.sebi.zuul.logic.items.Inventory;
import ch.sebi.zuul.logic.items.ItemSlot;
import ch.sebi.zuul.logic.items.items.food.Food;
import ch.sebi.zuul.logic.player.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Represents a fight
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class Fight {
    /**
     * The enemies of the fight
     */
    private ArrayList<Player> enemies = new ArrayList<>();
    /**
     * If the fight is still running
     */
    private boolean running = false;
    /**
     * The command parser which is used during the fight
     */
    private CommandParser commandParser;

    /**
     * constructor
     * @param enemies the enemies involed in the fight
     */
    public Fight(Player... enemies) {
        this.enemies.addAll(Arrays.asList(enemies));
        this.commandParser = new CommandParser(new CommandDiscoverer("fight", "ch.sebi.zuul"), IO.getParser());
    }

    /**
     * Starts the fight
     * @param game the game instance
     */
    public void play(Game game) {
        running = true;
        IO.println("You are in a fight");

        String enemiesStr = enemies.stream().map(e -> e.getName()).collect(Collectors.joining(", "));
        IO.println("You fight against " + enemiesStr);
        IO.println();
        IO.println("Type help to see what you can do in a battle");
        while (running) {
            IO.printf("> ");
            try {
                getCommandParser().parseNextCommand(game);
            } catch (IOException e) {
                IO.println(Color.RED, "Ah!! What just happend! I can't heer, see or fell anything...");
                e.printStackTrace();
            } catch (CommandNotFound commandNotFound) {
                IO.println(Color.RED, "ERROR: Unkown command. Type help to see what you can do.");
            }
        }
        IO.println("You won!");
        IO.println();
    }

    /**
     * Returns all enemies involved in the fight
     * @return the enemies
     */
    public Player[] getEnemies() {
        return enemies.toArray(new Player[0]);
    }

    /**
     * Returns the command parser used in the fight
     * @return the command parser
     */
    public CommandParser getCommandParser() {
        return commandParser;
    }

    /*
    * if the fight is still running
    */
    public boolean isRunning() {
        return running;
    }

    /**
     * Stops the fight
     */
    public void stop() {
        running = false;
    }

    /**
     * makes the next enemy turns
     * @param game the game instance
     */
    public void nextEnemyTurn(Game game) {
        if(Arrays.stream(getEnemies()).filter(e -> !e.isDead()).count() == 0) {
            running = false;
            return;
        }
        for(Player enemy : getEnemies()) {
            enemyTurn(enemy, game);
        }
    }

    /**
     * Makes the turn of one enemy
     * @param enemy the enemy which does the turn
     * @param game the game instance
     */
    private void enemyTurn(Player enemy, Game game) {
        IO.println();
        IO.println("Now is the turn of the enemy " + enemy.getName());
        boolean done = false;
        if(enemy.getLife() < 10) {
            //heal
            Inventory inventory = enemy.getInventory();
            for (int slotNr = 0; slotNr < inventory.getMaxSlots(); slotNr++) {
                ItemSlot slot = inventory.getSlot(slotNr);
                if(slot == null) continue;

                if(slot.getItem() instanceof Food) {
                    Food food = (Food) slot.getItem();
                    food.use(enemy, game);
                    if(food.destroyOnUse()) {
                        try {
                            inventory.incrementSlot(slotNr, -1);
                        } catch (IllegalSlotNumberException e) {
                            e.printStackTrace();
                        }
                    }
                    IO.printf("%s healed him self by %d to %d\n", enemy.getName(), food.getHeals(), enemy.getLife());
                    return;
                }
            }
        }
        //attack if the enemy couldn't healed him self
        if(enemy.getEquipedWeapon1() == null)  {
            IO.println("The enemy has no weapon");
            return;
        }
        enemy.getEquipedWeapon1().attack(game.getPlayer(), enemy, game);
        IO.printf("%s attacks you\n", enemy.getName());
        if(game.getPlayer().isDead()) {
            IO.println(Color.WHITE, Color.RED_BG, "You died!");
            System.exit(0);
        }
    }
}
