package ch.sebi.zuul.logic.condition;

import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.map.Room;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * a condition which checks if the player is in a set of rooms
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class RoomCondition implements Condition {
    /**
     * The rooms in which the condition evaluates to true
     */
    public ArrayList<Room> rooms = new ArrayList<>();

    /**
     * constructor
     * @param rooms the rooms in which the condition evaluates to true
     */
    public RoomCondition(Room... rooms) {
        this.rooms.addAll(Arrays.asList(rooms));
    }

    @Override
    public boolean condition(Game g) {
        Room currentRoom = g.getCurrentRoom();
        if(currentRoom == null) return false;
        for(Room r : rooms) {
            if(currentRoom.equals(r)) return true;
        }
        return false;
    }

    @Override
    public String getText() {
        return "The player is in one this rooms: " + rooms.stream().map(Room::getDescription).collect(Collectors.joining(", "));

    }
}
