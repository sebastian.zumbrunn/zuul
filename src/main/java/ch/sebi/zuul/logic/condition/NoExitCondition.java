package ch.sebi.zuul.logic.condition;

import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.map.Room;

/**
 * A condition which evaluates to true when the current room has no exits
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class NoExitCondition implements Condition {
    @Override
    public boolean condition(Game g) {
        Room r = g.getCurrentRoom();
        return r != null && r.getExits().length == 0;
    }

    @Override
    public String getText() {
        return "The current room has no exits";
    }
}
