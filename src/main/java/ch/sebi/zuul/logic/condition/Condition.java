package ch.sebi.zuul.logic.condition;

import ch.sebi.zuul.logic.Game;

/**
 * A interface which represents a condition.
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public interface Condition {
    /**
     * If the condition is truthy this function should return true, else false
     * @param g the game instance. From this instance is the current state of the game accessible
     * @return if the condition evaluated to true or false
     */
    boolean condition(Game g);

    /**
     * Returns what is needed to make the condition true
     * @return the text which is shown to the user
     */
    String getText();

}
