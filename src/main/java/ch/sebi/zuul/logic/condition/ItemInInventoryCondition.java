package ch.sebi.zuul.logic.condition;

import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.items.Inventory;
import ch.sebi.zuul.logic.items.Item;
import ch.sebi.zuul.logic.items.ItemSlot;

/**
 * A condition which checks if a given item is in the player's inventory
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class ItemInInventoryCondition implements Condition {
    /**
     * the needed item
     */
    public Item neededItem;
    /**
     * The minimal amount of the item
     */
    public int minimalAmount;

    /**
     * constructor
     *
     * @param neededItem    the needed item
     * @param minimalAmount the minimal amount of the item
     */
    public ItemInInventoryCondition(Item neededItem, int minimalAmount) {
        this.neededItem = neededItem;
        this.minimalAmount = minimalAmount;
    }

    @Override
    public boolean condition(Game g) {
        Inventory inventory = g.getPlayer().getInventory();
        int amount = 0;
        for (ItemSlot slot : inventory.getItemSlots()) {
            if (slot != null && slot.getItem() != null && slot.getItem().equals(neededItem)) {
                amount += slot.getAmount();
            }
        }
        return amount >= getMinimalAmount();
    }

    @Override
    public String getText() {
        return "The player needes the item " + getNeededItem().getName() + " " + getMinimalAmount() + " times";
    }

    /**
     * Returns the needed item
     *
     * @return the needed item
     */
    public Item getNeededItem() {
        return neededItem;
    }

    /**
     * Returns the minimial amount of the item which is needed to evaluate to true
     *
     * @return the minimal amount
     */
    public int getMinimalAmount() {
        return minimalAmount;
    }
}
