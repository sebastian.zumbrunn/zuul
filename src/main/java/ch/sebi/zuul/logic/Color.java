package ch.sebi.zuul.logic;

/**
 * An enum with all color
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public enum  Color {
    /**
     * The reset "color"
     */
    RESET("\u001B[0m", ""),
    /**
     * black
     */
    BLACK("\u001B[30m", ""),
    /**
     * red
     */
    RED("\u001B[31m", ""),
    /**
     * green
     */
    GREEN("\u001B[32m", ""),
    /**
     * yellow
     */
    YELLOW("\u001B[33m", ""),
    /**
     * blue
     */
    BLUE("\u001B[34m", ""),
    /**
     * purple
     */
    PURPLE("\u001B[35m", ""),
    /**
     * cyan
     */
    CYAN("\u001B[36m", ""),
    /**
     * white
     */
    WHITE("\u001B[37m", ""),

    /**
     * black background
     */
    BLACK_BG("\u001B[40m", ""),
    /**
     * red background
     */
    RED_BG("\u001B[41m", ""),
    /**
     * green background
     */
    GREEN_BG("\u001B[42m", ""),
    /**
     * yellow background
     */
    YELLOW_BG("\u001B[43m", ""),
    /**
     * blue background
     */
    BLUE_BG("\u001B[44m", ""),
    /**
     * purble background
     */
    PURPLE_BG("\u001B[45m", ""),
    /**
     * cayn background
     */
    CYAN_BG("\u001B[46m", ""),
    /**
     * white background
     */
    WHITE_BG("\u001B[47m", "");

    /**
     * The unix representation of the color
     */
    private String unixStr;
    /**
     * The linux representation of the color
     */
    private String windowStr;

    /**
     * constructor
     * @param unixStr the unix representation of the color
     * @param windowStr the windows representation of the color
     */
    Color(String unixStr, String windowStr) {
        this.unixStr = unixStr;
        this.windowStr = windowStr;
    }

    /**
     * Returns the unix representation of the color
     * @return the unix color
     */
    public String getUnixStr() {
        return unixStr;
    }

    /**
     * Returns the windows representation of the color
     * @return the windows color
     */
    public String getWindowStr() {
        return windowStr;
    }

    /**
     * Returns either the windows or the linux color, depending on the dedected platform
     * @return the "right" color
     */
    public String getPlatformStr() {
        String os = System.getProperty("os.name");
        if(os.contains("win")) {
            return getWindowStr();
        }
        return getUnixStr();
    }
}
