package ch.sebi.zuul.logic.player;

import ch.sebi.zuul.logic.items.BasicItem;
import ch.sebi.zuul.logic.items.Inventory;
import ch.sebi.zuul.logic.items.Weapon;

/**
 * A class which represents a player or any other character in the game
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class Player {
    /**
     * The name of the player
     */
    private String name;
    /**
     * how many lifes the player currently has
     */
    private int life;
    /**
     * How many lifes the player can have
     */
    private int maxLife;

    /**
     * The inventory of the player
     */
    private Inventory inventory;

    /**
     * the first equipped weapon. If it is null, nothing is equipped
     */
    private Weapon equipedWeapon1;
    /**
     * the second equipped weapon. If it is null, nothing is equipped
     */
    private Weapon equipedWeapon2;

    /**
     * constructor
     * @param name the name of the player
     * @param life how many lifes the player currently has
     * @param maxLife how many lifes the player can have
     * @param inventorySlots how many inventory slots the player should have
     */
    public Player(String name, int life, int maxLife, int inventorySlots) {
        this.name = name;
        this.life = life;
        this.maxLife = maxLife;
        this.inventory = new Inventory(inventorySlots);
    }

    /**
     * Returns the name of the player
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns how many lifes the player currently has
     * @return the lifes
     */
    public int getLife() {
        return life;
    }

    /**
     * Sets how many lifes the player has
     * @param life the lifes of the player
     */
    public void setLife(int life) {
        this.life = life;
    }

    /**
     * Increments the lifes of the player by the given amount.
     * If the life is greater than the {@link #maxLife} then the life is set to max life.
     * If the life is smaller than zero then the life is set to zero
     * @param by by how much the life is increast (or decreased if it is negative
     */
    public void incrementLife(int by) {
        this.life += by;
        if(this.getMaxLife() < life)
            this.life = getMaxLife();
        if(this.life < 0)
            this.life = 0;
    }


    /**
     * Returns the maximal amount of life the player can have
     * @return the max life
     */
    public int getMaxLife() {
        return maxLife;
    }

    /**
     * If the player is dead or not or
     * if the player's life are greate than zero
     * @return if the player is dead or not
     */
    public boolean isDead() {
        return getLife() <= 0;
    }

    /**
     * Returns the inventory of the player
     * @return the inventory
     */
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Returns the equipped weapon in the first slot
     * @return the weapon
     */
    public Weapon getEquipedWeapon1() {
        return equipedWeapon1;
    }

    /**
     * Sets the equipped weapon in the first slot
     * @param equipedWeapon1 the weapon
     */
    public void setEquipedWeapon1(Weapon equipedWeapon1) {
        this.equipedWeapon1 = (Weapon) equipedWeapon1;
    }

    /**
     * Returns the equipped weapon in the second slot
     * @return the weapon
     */
    public Weapon getEquipedWeapon2() {
        return equipedWeapon2;
    }

    /**
     * Sets the equipped weapon in the second slot
     * @param equipedWeapon2 the weapon
     */
    public void setEquipedWeapon2(Weapon equipedWeapon2) {
        this.equipedWeapon2 = equipedWeapon2;
    }
}
