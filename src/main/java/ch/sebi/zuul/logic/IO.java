package ch.sebi.zuul.logic;

import ch.sebi.zuul.command.Parser;

import java.io.InputStream;
import java.io.PrintStream;

/**
 * A class which contains the input and output stream for the game.
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class IO {
    /**
     * The output stream of the game.
     * For all outputs which are ment for the player should be printed using this {@link PrintStream}
     */
    private static PrintStream out = System.out;
    /**
     * The input stream of the game
     * For all input which are ment for the player should be printed using this {@link InputStream}
     */
    private static InputStream in = System.in;

    /**
     * The parser which should be use for all parsing
     */
    private static Parser parser = new Parser(in);

    /**
     * Returns the output stream of the game.
     * For all outputs which are ment for the player should be printed using this {@link PrintStream}
     *
     * @return the output stream
     */
    public static PrintStream getOut() {
        return out;
    }

    /**
     * Returns the input stream of the game
     * For all input which are ment for the player should be printed using this {@link PrintStream}
     *
     * @return the input stream
     */
    public static InputStream getIn() {
        return in;
    }

    /**
     * Returns the parser which uses the input stream of the game
     *
     * @return the parser
     */
    public static Parser getParser() {
        return parser;
    }

    /**
     * Prints an empty line in the {@link #out} stream
     */
    public static void println() {
        getOut().println();
    }

    /**
     * Prints the given string in the {@link #out} stream
     *
     * @param string the string which is printed
     */
    public static void println(String string) {
        getOut().println(string);
    }

    /**
     * Prints the given string in the {@link #out} stream
     *
     * @param color  sets the color of the text for this output
     * @param string the string which is printed
     */
    public static void println(Color color, String string) {
        getOut().println(color.getPlatformStr() + string + Color.RESET.getPlatformStr());
    }

    /**
     * Prints the given string in the {@link #out} stream
     *
     * @param color   sets the color of the text for this output
     * @param bgColor sets the color of the background for this text
     * @param string  the string which is printed
     */
    public static void println(Color color, Color bgColor, String string) {
        getOut().println(color.getPlatformStr() + bgColor.getPlatformStr() + string + Color.RESET.getPlatformStr());
    }

    /**
     * Prints the format with the args in the {@link #out} stream.<br/>
     * The function internally calls {@link PrintStream#printf(String, Object...)}.
     *
     * @param format the format
     * @param args   the arguments to the format
     * @see PrintStream#printf(String, Object...)
     */
    public static void printf(String format, Object... args) {
        getOut().printf(format, args);
    }

    /**
     * Prints the format with the args in the {@link #out} stream.<br/>
     * The function internally calls {@link PrintStream#printf(String, Object...)}.
     *
     * @param color  the color of the text
     * @param format the format
     * @param args   the arguments to the format
     * @see PrintStream#printf(String, Object...)
     */
    public static void printf(Color color, String format, Object... args) {
        getOut().printf(color.getPlatformStr() + format + Color.RESET.getPlatformStr(), args);
    }

    /**
     * Prints the format with the args in the {@link #out} stream.<br/>
     * The function internally calls {@link PrintStream#printf(String, Object...)}.
     *
     * @param color  the color of the text
     * @param bgColor the color of the background
     * @param format the format
     * @param args   the arguments to the format
     * @see PrintStream#printf(String, Object...)
     */
    public static void printf(Color color, Color bgColor, String format, Object... args) {
        getOut().printf(color.getPlatformStr() + bgColor.getPlatformStr() + format + Color.RESET.getPlatformStr(), args);
    }

}
