package ch.sebi.zuul.logic;

import ch.sebi.zuul.exceptions.CommandNotFound;
import ch.sebi.zuul.logic.fight.Fight;
import ch.sebi.zuul.logic.items.ItemSlot;
import ch.sebi.zuul.logic.map.Room;

import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The class which
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class MainGame {
    /**
     * The game instance
     */
    private Game game;

    /**
     * If the the game is still running
     */
    private boolean running = false;

    private boolean skipInformation = false;

    /**
     * constructor<br/>
     *  it creates a new game instance.
     */
    public MainGame() {
        game = new Game();
    }

    /**
     * Start the main game loop.
     * This method blocks until the thread is interrupted
     */
    public void play() {
        displayWelcome();
        running = true;
        try {
            while (!Thread.interrupted()) {
                proccessCommand();
            }
        } catch (IOException e) {
            IO.println("What did you do!");
            IO.println("I don't see or hear anything!!!!!!!");
            IO.println(e.toString());
        } finally {
            IO.println("Thank you for playing. Good bye.");
            running = false;
        }
    }

    /**
     * Displays the welcome message
     */
    private void displayWelcome() {
        IO.println();
        IO.println("Welcome to Adventure!");
        IO.println("Adventure is a new, incredibly boring adventure game.");
        IO.println("Type 'help' if you need help.");
        IO.println();
    }

    /**
     * Handels the command processing and prompting the user.
     * @throws IOException if while reading the input an error with the stream occurred
     */
    private void proccessCommand() throws IOException {
        printRoomWelcome();
        if(getGame().getCurrentRoom().getEnemies().length > 0) {
            Fight fight = new Fight(getGame().getCurrentRoom().getEnemies());
            getGame().setCurrentFight(fight);
            fight.play(game);
        }
        IO.printf("> ");
        try {
            getGame().getCommandParser().parseNextCommand(game);
        } catch (CommandNotFound commandNotFound) {
            IO.println(Color.RED, "ERROR: The command \"" + commandNotFound.getCmdWord() + "\" wasn't found");
            IO.println("Type \"help\" to get help");
            IO.println();
            setSkipInformation(true);
        }
    }

    /**
     * Prints informations about the current room
     */
    private void printRoomWelcome() {
        if(!areInformationSkipped()) {
            // checks if the player won or lost the game
            checkWinningConditions();
            checkLoosingConditions();

            IO.printf("You are in %s\n", getGame().getCurrentRoom().getDescription());

            String roomString = Arrays.stream(getGame().getCurrentRoom().getExits())
                    .map(e -> {
                        String name = e.getName();
                        if (e.getConditions().length == 1)
                            name += " (" + e.getConditions().length + " condition)";
                        else if (e.getConditions().length > 1)
                            name += " (" + e.getConditions().length + " conditions)";
                        return name;
                    }).collect(Collectors.joining(", "));

            IO.printf("Exits are %s\n", roomString);
            checkItemsInRoom();
        }

        setSkipInformation(false);
    }

    /**
     * Checks if the player has won or not. If he won, a message is printed and the game will exit
     */
    public void checkWinningConditions() {
        if(getGame().getWinningCondition().condition(getGame()))         {
            IO.println("You won the game! Congrats!");
            System.exit(0);
        }
    }

    /**
     * Checks if the player has lost or not. If he lost, a message is printed and the game will exit
     */
    public void checkLoosingConditions() {
        if(getGame().getLoosingCondition().condition(getGame())) {
            IO.println("You lost the game! F...");
            System.exit(0);
        }
    }

    /**
     * Checks if there are items in the current room.
     * If there are items, it will dipslay a list with all items in the current room to the user
     */
    public void checkItemsInRoom() {
        Room currentRoom = getGame().getCurrentRoom();
        ItemSlot[] items = currentRoom.getItems();
        if(items.length == 0) return;

        IO.println();
        IO.println("Found the following items:");
        for (int i = 0; i < items.length; i++) {
            IO.printf("\t%d) %s (%dx)\n", i + 1, items[i].getItem().getName(), items[i].getAmount());
        }
    }

    /**
     * Returns the game instance
     * @return the game instance
     */
    public Game getGame() {
        return game;
    }

    /**
     * Returns if the game is still running
     * To stop the game interupt the thread in which it is started
     * @return if the game is still running or not
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * If the information are skipped or not
     * @return if the information are shown or not
     */
    public boolean areInformationSkipped() {
        return skipInformation;
    }

    /**
     * Sets if the information about the current room, etc should be skipped
     * @param skipInformation if the information are shown to the player or not
     */
    public void setSkipInformation(boolean skipInformation) {
        this.skipInformation = skipInformation;
    }
}
