package ch.sebi.zuul.logic.map;

import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.condition.Condition;

import java.util.ArrayList;

/**
 * Represents an exit
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class Exit {
    /**
     * The name of the exit
     */
    private String name;
    /**
     * The room to which the exit goes
     */
    private Room room;

    /**
     * A list with all conditions which have to evaluate to true to make the exit usable
     */
    private ArrayList<Condition> conditions = new ArrayList<>();

    /**
     * constructor<br>
     * Represents an exit in the game. The name will be displayed to the user as the options to where he/she can go.
     *
     * @param name the name of the exit (for example: north, south or library, ...)
     * @param room the room itself
     */
    public Exit(String name, Room room) {
        this.name = name;
        this.room = room;
    }

    /**
     * Adds a new condition to the exit
     * @param condition the condition which has to evaluate to true to make the exit usable
     * @return
     */
    public Exit addCondition(Condition condition) {
        conditions.add(condition);
        return this;
    }

    /**
     * Returns the name which is displayed to the user to where he can go
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * returns the room to which the exit goes
     *
     * @return the room
     */
    public Room getRoom() {
        return room;
    }

    /**
     * Returns all conditions which have to evaluate to true to make the exit open
     * @return all conditions
     */
    public Condition[] getConditions() {
        return conditions.stream().toArray(Condition[]::new);
    }


    /**
     * Returns if the exit is open or not
     * @param game the game instance
     * @return if the exit is open
     */
    public boolean isOpen(Game game) {
        return conditions.stream().filter(c -> !c.condition(game)).count() == 0;
    }

    @Override
    public String toString() {
        return "Exit: " + getName();
    }
}
