package ch.sebi.zuul.logic.map;/*
 * Class ch.sebi.zuul.logic.map.Room - a room in an adventure game.
 *
 * This class is the main class of the "World of Zuul" application.
 * "World of Zuul" is a very simple, text based adventure game.
 *
 * A "ch.sebi.zuul.logic.map.Room" represents one location in the scenery of the game.  It is
 * connected to other rooms via exits.  The exits are labelled north,
 * east, south, west.  For each direction, the room stores a reference
 * to the neighboring room, or null if there is no exit in that direction.
 *
 * @author  Michael Kolling and David J. Barnes
 * @version 1.0 (February 2002)
 */

import ch.sebi.zuul.logic.items.Item;
import ch.sebi.zuul.logic.items.ItemSlot;
import ch.sebi.zuul.logic.player.Player;

import java.util.*;

/**
 * A class which represents a room.
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class Room {
    /**
     * the description of the room
     */
    private String description;

    /**
     * the exits to where the user can go from this room
     */
    private Map<String, Exit> exits = new LinkedHashMap<>();

    /**
     * A list with all items which are in the room
     */
    private ArrayList<ItemSlot> items = new ArrayList<>();

    private ArrayList<Player> enemies = new ArrayList<>();

    /**
     * Create a room described "description". Initially, it has
     * no exits. "description" is something like "a kitchen" or
     * "an open court yard".
     */
    public Room(String description) {
        this.description = description;
    }

    /**
     * Adds an exits to the room
     *
     * @param e the exit
     */
    public void addExit(Exit e) {
        exits.put(e.getName(), e);
    }

    /**
     * Adds an exit with the given name and room
     * to the room
     *
     * @param name the name which is displayed to the user where he can go
     * @param room the room to where the exit goes
     */
    public Exit addExit(String name, Room room) {
        Exit e = new Exit(name, room);
        addExit(e);
        return e;
    }

    /**
     * Returns all exits to where the user can go from this room
     *
     * @return the exits
     */
    public Exit[] getExits() {
        return exits.values().toArray(new Exit[0]);
    }

    /**
     * Returns the exit if it exits or null
     *
     * @param name the name of the eixt
     * @return the exit or null
     */
    public Exit getExit(String name) {
        return exits.get(name);
    }

    /**
     * Return the description of the room (the one that was defined
     * in the constructor).
     */
    public String getDescription() {
        return description;
    }

    /**
     * Adds an item to this room
     *
     * @param i      the item
     * @param amount how many of the item should be added
     */
    public void addItem(Item i, int amount) {
        if (!containsItem(i)) {
            items.add(new ItemSlot(0, i));
        }
        ItemSlot place = getItemSlot(i);
        place.setAmount(place.getAmount() + amount);
    }

    /**
     * Decrements the amount counter of an item until it is zero or below.
     * If this point is reached then the item slot is removed from the room
     *
     * @param i      the item
     * @param amount the amount by which is is removed
     * @return
     */
    public boolean removeItem(Item i, int amount) {
        if (!containsItem(i)) {
            return false;
        }

        ItemSlot slot = getItemSlot(i);
        slot.setAmount(slot.getAmount() - amount);
        if (slot.getAmount() <= 0) {
            items.remove(slot);
        }
        return true;
    }

    /**
     * Checks if an item is present in this room
     *
     * @param item the item
     * @return if the item is in this room
     */
    public boolean containsItem(Item item) {
        return getItemSlot(item) != null;
    }

    /**
     * Returns the item slot for the given item or null if the item does not exist
     *
     * @param item the item for which the item slot is returned
     * @return the item slot or null if the item is not present in this room
     */
    private ItemSlot getItemSlot(Item item) {
        if (item == null) return null;
        for (ItemSlot i : items) {
            if (item.equals(i.getItem()))
                return i;
        }
        return null;
    }

    /**
     * Returns all items present in this room
     *
     * @return all items
     */
    public ItemSlot[] getItems() {
        return items.toArray(new ItemSlot[0]);
    }

    /**
     * Adds enemies to the room
     *
     * @param enemies the enemies which are added
     */
    public void addEnemy(Player... enemies) {
        this.enemies.addAll(Arrays.asList(enemies));
    }

    /**
     * Removes an enemy from the room
     * @param enemy removes the enemy
     */
    public void removeEnemy(Player enemy) {
        enemies.remove(enemy);
    }

    /**
     * Returns all enemies in this room
     *
     * @return
     */
    public Player[] getEnemies() {
        return enemies.toArray(new Player[0]);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return Objects.equals(description, room.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description);
    }
}
