package ch.sebi.zuul.logic.items;

import ch.sebi.zuul.exceptions.IllegalSlotNumberException;
import ch.sebi.zuul.exceptions.NoSpaceException;

import java.util.HashMap;

/**
 * An inventory
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class Inventory {
    /**
     * The slots mapped to their slot number
     */
    private HashMap<Integer, ItemSlot> slots = new HashMap<>();
    /**
     * the max allowed numbers of slots
     */
    private int maxSlots;

    /**
     * constructor
     *
     * @param maxSlots the max numbers of slots allowed in this inventory
     */
    public Inventory(int maxSlots) {
        this.maxSlots = maxSlots;
    }

    /**
     * Creates a new slot where there is space. If there is no space left, then a {@link NoSpaceException} is thrown.
     * The newly created slot is filled with the given item by the given amount
     *
     * @param item   the item which is in the new slot
     * @param amount how many items are in the new slot
     * @throws NoSpaceException if there is now space left for a new slot
     */
    public void setNewSlot(Item item, int amount) throws NoSpaceException {
        for (int i = 0; i < getMaxSlots(); i++) {
            if (getSlot(i) == null) {
                try {
                    setSlot(i, item, amount);
                } catch (IllegalSlotNumberException e) {
                    e.printStackTrace();
                }
                return;
            }
        }
        throw new NoSpaceException(getMaxSlots());
    }

    /**
     * Creates a new slot an sets it to the given slot number
     *
     * @param slot   the slot number/id
     * @param i      the item which is stored in the slot
     * @param amount the amount of this kind of item
     * @throws IllegalSlotNumberException if the given slot number is higher than the max slot number or lower than zero
     */
    public void setSlot(int slot, Item i, int amount) throws IllegalSlotNumberException {
        if (slot < 0 || slot >= maxSlots) {
            throw new IllegalSlotNumberException(slot, maxSlots);
        }
        slots.put(slot, new ItemSlot(amount, i));
    }

    /**
     * Removes a slot from the inventory
     *
     * @param slot the slot number of the slot to remove
     * @throws IllegalSlotNumberException if the given slot number is higher than the max slot number or lower than zero
     */
    public void removeSlot(int slot) throws IllegalSlotNumberException {
        if (slot < 0 || slot >= maxSlots) {
            throw new IllegalSlotNumberException(slot, maxSlots);
        }
        slots.remove(slot);
    }

    /**
     * Updates the slot with the given slot id with the new amount
     *
     * @param slot   the slot id which is updated
     * @param amount the new amount which is saved to slot
     * @return if the operation was successful or not
     * @throws IllegalSlotNumberException if the given slot number does not exist or is higher than the max slot number
     *                                    or lower than zero
     */
    public boolean updateSlot(int slot, int amount) throws IllegalSlotNumberException {
        if (slot < 0 || slot >= maxSlots) {
            throw new IllegalSlotNumberException(slot, maxSlots);
        }
        ItemSlot place = slots.get(slot);
        if (place == null)
            throw new IllegalSlotNumberException(slot, maxSlots);
        place.setAmount(amount);
        return true;
    }

    /**
     * Increments the amount of items in the given slot by the given amount
     *
     * @param slot   the slot whoms amount is increased
     * @param amount the amount by which it is increased
     * @throws IllegalSlotNumberException
     */
    public void incrementSlot(int slot, int amount) throws IllegalSlotNumberException {
        if (slot < 0 || slot >= maxSlots) {
            throw new IllegalSlotNumberException(slot, maxSlots);
        }
        ItemSlot place = slots.get(slot);
        if (place == null)
            throw new IllegalSlotNumberException(slot, maxSlots);
        place.setAmount(place.getAmount() + amount);
        if(place.getAmount() <= 0) {
            removeSlot(slot);
        }
    }

    /**
     * Finds the slot id for the given item
     *
     * @param item the item which should  be found
     * @return the slot id or -1 if the slot coundn't be found
     */
    public int findItem(Item item) {
        if (item == null) return -1;
        for (int i = 0; i < slots.size(); i++) {
            ItemSlot slot = getSlot(i);
            if (slot != null && item.equals(slot.getItem())) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Returns the slot with the given slot number or null if the slot doesn't exist yet
     *
     * @param i the slot number of the slot
     * @return the slot or null
     */
    public ItemSlot getSlot(int i) {
        return slots.get(i);
    }

    /**
     * Returns all slots stored in the inventory
     *
     * @return all slots
     */
    public ItemSlot[] getItemSlots() {
        return slots.values().toArray(new ItemSlot[0]);
    }

    /**
     * Returns the max slot number valid in this inventory
     *
     * @return the max slot number
     */
    public int getMaxSlots() {
        return maxSlots;
    }

    /**
     * Sets a new max slot number for the inventory.
     * If the new slot number is smaller than the old
     * then all slots which are not reachable anymore are deleted
     *
     * @param maxSlots the max slot number valid in the inventory
     */
    public void setMaxSlots(int maxSlots) {
        if (this.maxSlots > maxSlots) {
            // removes all slots which are not reachable anymore
            for (int i = maxSlots; i < this.maxSlots; i++) {
                slots.remove(i);
            }
        }
        this.maxSlots = maxSlots;
    }
}
