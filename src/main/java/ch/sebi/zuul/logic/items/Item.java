package ch.sebi.zuul.logic.items;

/**
 * An interface which implements the basic of an item
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public interface Item {
    /**
     * Returns the name of the item
     * @return the name
     */
    String getName();
}
