package ch.sebi.zuul.logic.items.items.weapon;

/**
 * A class which represents a sword
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class Sword extends BasicWeapon {
    /**
     * constructor<br/>
     * creates a sword
     * @param damage the damage of the sword
     */
    public Sword(int damage) {
        super("Sword", damage);
    }

}
