package ch.sebi.zuul.logic.items;

import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.player.Player;

/**
 * If a class extends from this class, then it can be used as a weapon in battle
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public interface Weapon extends Item{
    /**
     * Invoked if the weapon is being used
     * @param enemy the enemy which is attacked
     * @param attacker the player which is attacking the enemy
     * @param game the game instance
     */
    void attack(Player enemy, Player attacker, Game game);
}
