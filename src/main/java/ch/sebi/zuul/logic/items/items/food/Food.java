package ch.sebi.zuul.logic.items.items.food;

import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.items.BasicItem;
import ch.sebi.zuul.logic.items.Usable;
import ch.sebi.zuul.logic.player.Player;

/**
 * An class which represents food items
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class Food extends BasicItem implements Usable {
    /**
     * the amount the food heals
     */
    private int heals;

    /**
     * constructor
     * @param name the name of the food
     * @param heals how much the food heals
     */
    public Food(String name, int heals) {
        super(name);
        this.heals = heals;
    }

    /**
     * Returns how much the food heals
     * @return how much it heals
     */
    public int getHeals() {
        return heals;
    }

    @Override
    public void use(Player player, Game game) {
        player.incrementLife(heals);
    }

    @Override
    public boolean destroyOnUse() {
        return true;
    }
}
