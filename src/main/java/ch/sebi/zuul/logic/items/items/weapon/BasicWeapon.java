package ch.sebi.zuul.logic.items.items.weapon;

import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.IO;
import ch.sebi.zuul.logic.items.BasicItem;
import ch.sebi.zuul.logic.items.Weapon;
import ch.sebi.zuul.logic.player.Player;

/**
 * A basic weapon which makes damage
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class BasicWeapon extends BasicItem implements Weapon {
    /**
     *  how much damage the weapon makes
     */
    private int damage;

    /**
     * constructor
     *
     * @param name the name of the item
     */
    public BasicWeapon(String name, int damage) {
        super(name);
        this.damage = damage;
    }

    /**
     * Returns the damage the weapon makes
     * @return the damage
     */
    public int getDamage() {
        return damage;
    }

    @Override
    public void attack(Player enemy, Player attacker, Game game) {
        enemy.incrementLife(-getDamage());
        IO.printf(getAttackedMessage(enemy, attacker, game));
    }

    /**
     * Returns the message which is printed when the weapon attacks an enemy.
     * The message is printed in a printf, meaning if you want a line ending at the end, then add a "\n" in the string
     * @param enemy the enemy
     * @param attacker the attacker
     * @param game the game instance
     * @return the string which is outputted
     */
    protected String getAttackedMessage(Player enemy, Player attacker, Game game) {
        return String.format("%s attacked %s and made %d damage\n", attacker.getName(), enemy.getName(), getDamage());
    }
}
