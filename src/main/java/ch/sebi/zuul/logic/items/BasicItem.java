package ch.sebi.zuul.logic.items;

import java.util.Objects;

/**
 * A class which represents an item
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public abstract class BasicItem implements Item {
    /**
     * the name of the item
     */
    private String name;

    /**
     * constructor
     * @param name the name of the item
     */
    public BasicItem(String name) {
        this.name = name;
    }

    /**
     * Returns the name of the item
     * @return the name
     */
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BasicItem item = (BasicItem) o;
        return Objects.equals(name, item.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
