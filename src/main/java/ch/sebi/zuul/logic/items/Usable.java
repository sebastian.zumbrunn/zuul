package ch.sebi.zuul.logic.items;

import ch.sebi.zuul.logic.Game;
import ch.sebi.zuul.logic.player.Player;

/**
 * A interface which annotates a if an item is usable
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public interface Usable extends Item {
    /**
     * called if the item is used
     * @param player the character which uses the item
     * @param game the game instance
     */
    void use(Player player, Game game);

    /**
     * Returns if the item is destroyed on use
     * @return if the item is destroy on use or no
     */
    boolean destroyOnUse();
}
