package ch.sebi.zuul.logic.items;

/**
 * An item slot represents an item with the amount of that item which is stored in the slot
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class ItemSlot {
    /**
     * the amount of the item
     */
    private int amount;
    /**
     * the item itself
     */
    private Item item;

    /**
     * constructor
     * @param amount the amount of the item
     * @param item the item itself
     */
    public ItemSlot(int amount, Item item) {
        this.amount = amount;
        this.item = item;
    }

    /**
     * Returns the amount of the item which is stored
     * @return the amount
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Sets  how much of the item is stored
     * @param amount the new amount
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * Returns the item of the slot
     * @return the item
     */
    public Item getItem() {
        return item;
    }
}
