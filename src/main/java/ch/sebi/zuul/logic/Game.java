package ch.sebi.zuul.logic;

import ch.sebi.zuul.command.CommandDiscoverer;
import ch.sebi.zuul.command.CommandParser;
import ch.sebi.zuul.logic.condition.ItemInInventoryCondition;
import ch.sebi.zuul.logic.condition.NoExitCondition;
import ch.sebi.zuul.logic.condition.RoomCondition;
import ch.sebi.zuul.logic.condition.Condition;
import ch.sebi.zuul.logic.fight.Fight;
import ch.sebi.zuul.logic.items.items.food.Food;
import ch.sebi.zuul.logic.items.items.weapon.Sword;
import ch.sebi.zuul.logic.map.Room;
import ch.sebi.zuul.logic.player.Player;

/**
 * This class is the main data class for the game.
 * It contains all information about the current state of the game<br/><br/>
 * <p>
 * This main class creates and initialises all the others: it creates all
 * rooms, creates the parser and starts the game.  It also evaluates and
 * executes the commands that the parser returns.
 *
 * @author Michael Kolling and David J. Barnes and Sebastian Zumbrunn
 * @version 1.1
 */

public class Game {
    private Room currentRoom;

    private Condition winningCondition;
    private Condition loosingCondition;

    private Player player;

    private CommandParser commandParser;

    private Fight currentFight;

    /**
     * Create the game and initialise its internal map.
     */
    public Game() {
        createRooms();
        this.commandParser = new CommandParser(new CommandDiscoverer("default", "ch.sebi.zuul"), IO.getParser());
        this.player = new Player("Sebi", 10, 10, 10);
    }

    /**
     * Create all the rooms and link their exits together.
     */
    private void createRooms() {
        // create the rooms
        Room room1 = new Room("University Room 1");
        Room room2 = new Room("University Room 2");

        Room library = new Room("Library");
        Room trap = new Room("A trap. Damm!");
        Room mainEntrance = new Room("Main Entrance");
        mainEntrance.addEnemy(new Player("Ralf", 4, 10, 1));

        Room migros = new Room("Migros");
        migros.addItem(new Food("Cola", 2), 2);
        Room park = new Room("A park");
        park.addItem(new Sword(3), 1);
        Room trainStation = new Room("SBB Train Station");

        room1.addExit("Entrance", mainEntrance);
        room1.addExit("Library", library);

        room2.addExit("Entrance", mainEntrance);
        room2.addExit("Library", library);

        mainEntrance.addExit("Room1", room1);
        mainEntrance.addExit("Room2", room2);
        mainEntrance.addExit("Library", library);
        mainEntrance.addExit("Migros", migros);
        mainEntrance.addExit("Park", park);

        library.addExit("Entrance", mainEntrance);
        library.addExit("Sweets", trap);

        park.addExit("Entrance", mainEntrance);
        park.addExit("Migros", migros);
        park.addExit("SBB", trainStation);

        migros.addExit("Entrance", mainEntrance).addCondition(new ItemInInventoryCondition(new Food("Cola", 2), 1));
        migros.addExit("Park", park);

        trainStation.addExit("Park", park);

        currentRoom = trainStation;

        winningCondition = new RoomCondition(room1, room2);
        loosingCondition = new NoExitCondition();
    }


    /**
     * Returns the current room
     *
     * @return the room
     */
    public Room getCurrentRoom() {
        return currentRoom;
    }

    /**
     * Sets the current room to the given room
     *
     * @param currentRoom the new room where the
     */
    public void setCurrentRoom(Room currentRoom) {
        this.currentRoom = currentRoom;
    }

    /**
     * Returns the current player
     *
     * @return the player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Returns the winning winningCondition which has to evaluate to true
     *
     * @return the winningCondition
     */
    public Condition getWinningCondition() {
        return winningCondition;
    }


    /**
     * Returns the loosing condition which has to evaluate to true to loos
     * the game.
     *
     * @return the loosing condition
     */
    public Condition getLoosingCondition() {
        return loosingCondition;
    }

    /**
     * Returns the main command parser
     *
     * @return the main command parser
     */
    public CommandParser getCommandParser() {
        return commandParser;
    }

    /**
     * Returns the current fight or null there is no fight currently
     * @return the fight instance or null
     */
    public Fight getCurrentFight() {
        return currentFight;
    }

    /**
     * Sets the current fight. If it is null, there is no current fight
     * @param currentFight the current fight instance or nullull     */
    public void setCurrentFight(Fight currentFight) {
        this.currentFight = currentFight;
    }
}
