package ch.sebi.zuul.exceptions;

/**
 * An exception which occures if an input fails in {@link ch.sebi.zuul.logic.ui.input.Input#convertInput(String)}
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class InputConvertException extends Exception {
    /**
     * if the error message should be shown
     */
    private boolean showErrorMsg;

    /**
     * constructor<br/>
     * @param errorMsg the error message
     * @param showErrorMsg if the message is whoen
     */
    public InputConvertException(String errorMsg, boolean showErrorMsg) {
        super(errorMsg);
        this.showErrorMsg = showErrorMsg;
    }

    /**
     * constructor<br/>
     * @param errorMsg the error message
     * @param showErrorMsg if the message is whoen
     * @param cause the real exception which caused this exception
     */
    public InputConvertException(String errorMsg, boolean showErrorMsg, Throwable cause) {
        super(errorMsg, cause);
        this.showErrorMsg = showErrorMsg;
    }

    /**
     * Returns if the error message should be shown in the input
     * @return if the message gets shown
     */
    public boolean isShowErrorMsg() {
        return showErrorMsg;
    }
}
