package ch.sebi.zuul.exceptions;

import ch.sebi.zuul.logic.items.Inventory;

/**
 * An exception which is thrown if someone tries to access/allocate/create/... a slot in the inventory which
 * is either greater or equal than the max slot number of the Inventory ({@link Inventory#getMaxSlots()}, if it is less
 * then zero or if the accessed slot is null and not accessible.
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class IllegalSlotNumberException extends InventoryException {
    /**
     * constructor
     * @param slot the slot number which is accessed
     * @param maxSlots the max number of slots valid in the inventory
     */
    public IllegalSlotNumberException(int slot, int maxSlots) {
        super("Illegal slot number " + slot + " - max slot number is " + maxSlots);
    }
}
