package ch.sebi.zuul.exceptions;

/**
 * A exception base class. All exception which implement this class are somehow related to the inventory
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public abstract class InventoryException extends Exception {
    /**
     *  constructor
     */
    public InventoryException() {
    }

    /**
     * constructor
     *
     * @param message the messesage which is displayed by the exception
     */
    public InventoryException(String message) {
        super(message);
    }

    /**
     * constructor
     * @param message the messesage which is displayed by the exception
     * @param cause the real cause which cased throwing this exception
     */
    public InventoryException(String message, Throwable cause) {
        super(message, cause);
    }
}
