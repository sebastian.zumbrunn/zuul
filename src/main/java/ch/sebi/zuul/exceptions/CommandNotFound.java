package ch.sebi.zuul.exceptions;

/**
 * An exception which is thrown if a command wasn't found by the {@link ch.sebi.zuul.command.CommandParser}
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class CommandNotFound extends Exception {
    /**
     * The cmd which wasn't found
     */
    private String cmdWord;

    /**
     * constructor
     * @param cmdWord the cmd which wasn't found
     */
    public CommandNotFound(String cmdWord) {
        super("Unknown command \"" + cmdWord + "\"");
        this.cmdWord = cmdWord;
    }

    /**
     * The command which wasn't found
     * @return the command
     */
    public String getCmdWord() {
        return cmdWord;
    }
}
