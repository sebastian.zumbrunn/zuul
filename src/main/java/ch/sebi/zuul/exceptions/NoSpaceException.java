package ch.sebi.zuul.exceptions;


/**
 * An exception which is thrown if no space is left in the inventory and the user tries to allocate a new slot
 *
 * @author Sebastian Zumbrunn
 * @version 1.0
 */
public class NoSpaceException extends InventoryException {
    /**
     * constructor
     * @param maxSlots the max numbers of slots
     */
    public NoSpaceException(int maxSlots) {
        super("Could not allocate space for a new inventory slot; max number of slots is " + maxSlots);
    }
}
